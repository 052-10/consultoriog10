
function mostrar(){
	const modalProducto = document.getElementById("btnMostrar");
	modalProducto.click();
}

function salir(){
    sessionStorage.removeItem("usuario")
    location.href="../login/index.html"
}

function EditarPaciente(nombrePaciente) {
    const modalProducto = document.getElementById("btnMostrar");
    document.getElementById("btnRegistrar").innerHTML = "Editar";
    let peticion = sendRequest('producto/buscar?nombre=' + nombrePaciente, 'GET', '');
    peticion.onload = function () {
        let datos = peticion.response;
        let ProductoEditar = datos[0];
        document.getElementById("txtNombreProducto").value = ProductoEditar.nombreProducto;
        document.getElementById("txtPrecioCompra").value = ProductoEditar.valorCompra;
        document.getElementById("txtPrecioVenta").value = ProductoEditar.valorVenta;
        document.getElementById("txtCantidad").value = ProductoEditar.cantidad;
        document.getElementById("txtIdProducto").value = ProductoEditar.idProducto;
        modalProducto.click();
    }
    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al consultar el producto',
            showConfirmButton: false,
            timer: 1000
        })
    }
}

function guardarPaciente() {
    let codigoInterno = document.getElementById("txtCodigoInterno").value;
    let nombre = document.getElementById("txtNombre").value;
    let apellido = document.getElementById("txtApellido").value;
    let genero = document.getElementById("txtGenero").value;
    let direccion = document.getElementById("txtDireccion").value;
    let telefono = document.getElementById("txtTelefono").value;
    let email = document.getElementById("txtEmail").value;
    let alergia = document.getElementById("txtAlergia").value;
    let enfermedadPadecida = document.getElementById("txtEnfermedadPadecida").value;
    let estado = document.getElementById("txtEstado").value;
    let fechaCreacion = document.getElementById("txtFechaCreacion").value;
    let fechaNacimiento = document.getElementById("txtFechaNacimiento").value;
    let medicamentoRequerido = document.getElementById("txtMedicamentoRequerido").value;
    let idPaciente = document.getElementById("txtIdPaciente").value;
    let data = {};
    let peticion = "";
    let Mensaje="";
    console.log(idPaciente)
    if (idPaciente.length > 0) {
        console.log("Paso actualziar")
        data = {
            "id": idPaciente,
            "codigoInterno": codigoInterno,
            "nombrePaciente": nombre,
            "apellidoPaciente": apellido,
            "genero": genero,
            "direccion": direccion,
            "telefono": telefono,
            "email": email,
            "alergia": alergia,
            "enfermedadPadecida": enfermedadPadecida,
            "estaActivo": estado,
            "fechaCreacion": fechaCreacion,
            "fechaNacimiento": fechaNacimiento,
            "medicamentoRequerido": medicamentoRequerido
        };
        //actualiza
        peticion = sendRequest('paciente', 'PUT', data);
        Mensaje="Paciente Editado Exitosamente";
    } else {
        console.log("Paso guardar")
        data = {
          
            "codigoInterno": codigoInterno,
            "nombrePaciente": nombre,
            "apellidoPaciente": apellido,
            "genero": genero,
            "direccion": direccion,
            "telefono": telefono,
            "email": email,
            "alergia": alergia,
            "enfermedadPadecida": enfermedadPadecida,
            "estaActivo": estado,
            "fechaCreacion": fechaCreacion,
            "fechaNacimiento": fechaNacimiento,
            "medicamentoRequerido": medicamentoRequerido
        };
        //guarda
        peticion = sendRequest('paciente', 'POST', data);
        Mensaje="Paciente Creado Exitosamente";
    }


    peticion.onload = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: Mensaje,
            showConfirmButton: false,
            timer: 1000
        }
        ).then((result) => {
            // Reload the Page
            document.getElementById("txtNombre").value = "";
           // location.reload();
        })
    }
    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al Guardar el producto',
            showConfirmButton: false,
            timer: 1000
        })
    }
}

function EliminarPaciente(idPaciente) {
    Swal.fire({
        title: 'Esta Seguro?',
        text: "Desea eliminar a este Producto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar!'
    }).then((result) => {
        if (result.isConfirmed) {
            let peticion = sendRequest('paciente/' + idPaciente, 'DELETE', '');
            peticion.onload = function () {
                Swal.fire({
                    position: 'top-end',
                    icon: 'warning',
                    title: 'Producto Eliminado',
                    showConfirmButton: false,
                    timer: 1000
                }
                ).then((result) => {
                    location.reload();
                })
            }
            peticion.onerror = function () {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error al Eliminar el paciente',
                    showConfirmButton: false,
                    timer: 1000
                })
            }

        }
    })
}

function cargarSelectProductos(){
    let peticion = sendRequest('producto/lista', 'GET', '');
    let productos = document.getElementById('selectProductos');
    peticion.onload = function () {
        let datos = peticion.response;
        datos.forEach(producto => {
            let opcion=document.createElement("option");
            opcion.text=producto.nombreProducto;
            opcion.value=producto.idProducto;
            productos.add(opcion);
        });
    }

}


function listarPacientes() {

    /*INicio Mostrar nombre      usuario*/ 
    document.getElementById("lblNombreUsuario").innerHTML=sessionStorage.getItem("nombre");

    /*Fin Nombre Usuario*/

    //Especificamos el tipo de peticion
    let peticion = sendRequest('paciente/lista', 'GET', '');
    //Creamos una variable que haga referencia a tabla creada en html con id listadoPaciente

    let tablaPaciente = document.getElementById('listadoPaciente');
    //Si la paticion cargo bien, cargo los datos a la tabla
    peticion.onload = function () {
        //Guardo los datos que me da en la variable datos
        let datos = peticion.response;
      
        //recorro la tabla paciente
        datos.forEach(paciente => {
            tablaPaciente.innerHTML += `
                <tr>
                    <td>${paciente.codigoInterno}</td>
                    <td>${paciente.nombrePaciente}</td>
                    <td>${paciente.apellidoPaciente}</td>
                    <td>${paciente.genero}</td>
                    <td>${paciente.direccion}</td>
                    <td>${paciente.email}</td>
                    <td>    
                        <button type="button" onclick="EliminarPaciente(${paciente.id})" class="btn btn-danger">Eliminar</button>
                        <button type="button" onclick="nombrePaciente('${paciente.nombrePaciente}')" class="btn btn-success">Editar</button>
                    </td>
                </tr>
`
        });
    }

    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al cargar productos',
            showConfirmButton: false,
            timer: 1000
        })
    }

}




