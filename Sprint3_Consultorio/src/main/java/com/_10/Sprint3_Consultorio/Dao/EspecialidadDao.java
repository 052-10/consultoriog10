
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Breyner Payares
 */
public interface EspecialidadDao extends JpaRepository<Especialidad, Integer> {
    
}
