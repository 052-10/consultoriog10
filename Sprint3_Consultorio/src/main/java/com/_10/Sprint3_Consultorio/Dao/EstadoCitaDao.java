
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.EstadoCita;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Breyner Payares
 */
public interface EstadoCitaDao extends JpaRepository<EstadoCita, Integer> {
    
}
