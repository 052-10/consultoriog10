
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.EstadoPago;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Breyner Payares
 */
public interface EstadoPagoDao extends JpaRepository<EstadoPago, Integer> {
    
}
