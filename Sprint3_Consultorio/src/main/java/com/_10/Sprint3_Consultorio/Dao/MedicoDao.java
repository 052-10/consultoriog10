
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.Medico;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Breyner payares
 */
public interface MedicoDao extends JpaRepository<Medico, Integer>{
    
    @Query(value="select m.* from medico m inner join especialidad e on e.id=m.id_especialidad WHERE m.nombre_medico = :nombreMedico",nativeQuery = true)
    List<Medico> searchBynombreMedico(@Param("nombreMedico") String nombreMedico);
}
