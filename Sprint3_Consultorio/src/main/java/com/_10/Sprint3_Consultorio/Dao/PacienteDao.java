/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.Paciente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author orlando
 */
public interface PacienteDao extends JpaRepository<Paciente, Integer>{
    /*
    //Buscar productos que cumplan con una condicion de Igual
    List<Paciente> findByvalorCompra(double disponibilidad);
    
    //Buscar productos que contengan una parte del nombre
    List<Paciente> findBynombreProductoContains(String nombre);
    
    //Buscar Productos por un precio entre datos
    List<Paciente> findByvalorVentaBetween(double precioInicial,double precioFinal);
    
    //Buscar Atravez de una Consulta
    @Query(value="SELECT * FROM producto p WHERE p.nombreproducto = :usuario",nativeQuery = true)
    List<Paciente> searchBynombreProductoStartsWith(@Param("usuario") String usuario);
    */
}
