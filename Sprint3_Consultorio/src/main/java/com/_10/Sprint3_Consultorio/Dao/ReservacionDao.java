/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com._10.Sprint3_Consultorio.Dao;

import com._10.Sprint3_Consultorio.model.Paciente;
import com._10.Sprint3_Consultorio.model.Reservacion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author orlando
 */
public interface ReservacionDao extends JpaRepository<Reservacion, Integer> {
    
}
