/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.Dao;


import com._10.Sprint3_Consultorio.model.Rol;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author orlando
 */
public interface RolDao  extends JpaRepository<Rol, Integer> {
    
    
    
    //Buscar perfil que contengan una parte del nombre perfil
    List<Rol> findByNombreRol(String nombre);
    
   
}
