/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com._10.Sprint3_Consultorio.Dao;


import com._10.Sprint3_Consultorio.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author orlando
 */
public interface UserDao extends JpaRepository<User, Integer> {
    @Query(value="select * from user where email=:email  and password=:password",nativeQuery=true)
    User login(@Param("email") String email, @Param("password") String password);
}
