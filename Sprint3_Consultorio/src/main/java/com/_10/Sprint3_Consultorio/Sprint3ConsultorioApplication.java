package com._10.Sprint3_Consultorio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprint3ConsultorioApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sprint3ConsultorioApplication.class, args);
	}

}
