
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.Especialidad;
import com._10.Sprint3_Consultorio.service.EspecialidadServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Breyner Payares
 */

@RestController //va a recibir peticiones
@CrossOrigin("*")
@RequestMapping("/especialidad")
public class EspecialidadControlador {
    
    @Autowired
    private EspecialidadServicio especialidadServicio;
    
    @GetMapping("/lista")
    public List<Especialidad> consultarTodo(){
        return especialidadServicio.listarEspecialidad();
    }
    
    @PostMapping
    public Especialidad insertar(@RequestBody Especialidad nuevaEsp) {
        return especialidadServicio.save(nuevaEsp);
    }
    
    @DeleteMapping(path = "/{id}") 
    public String eliminar(@PathVariable Integer id) {
        try {
            especialidadServicio.delete(id);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }
    
    @PutMapping
    public Especialidad actualizar(@RequestBody Especialidad especialidadModificar) {
        return especialidadServicio.actualizar(especialidadModificar);
    }
}
