
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.EstadoCita;
import com._10.Sprint3_Consultorio.service.EstadoCitaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Breyner Payares
 */

@RestController //va a recibir peticiones
@CrossOrigin("*")
@RequestMapping("/estadocita")
public class EstadoCitaControlador {
    
    @Autowired
    private EstadoCitaServicio estadoCitaServicio;
    
    @GetMapping("/lista")
    public List<EstadoCita> consultarTodo(){
        return estadoCitaServicio.listarEstadoCita();
    }
    
    @PostMapping
    public EstadoCita insertar(@RequestBody EstadoCita nuevoEst) {
        return estadoCitaServicio.save(nuevoEst);
    }
    
    @DeleteMapping(path = "/{id}") 
    public String eliminar(@PathVariable Integer id) {
        try {
            estadoCitaServicio.delete(id);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }
    
    @PutMapping
    public EstadoCita actualizar(@RequestBody EstadoCita estadoCitaModificar) {
        return estadoCitaServicio.actualizar(estadoCitaModificar);
    }
}
