
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.EstadoPago;
import com._10.Sprint3_Consultorio.service.EstadoPagoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Breyner Payares
 */

@RestController //va a recibir peticiones
@CrossOrigin("*")
@RequestMapping("/estadopago")
public class EstadoPagoControlador {
    
    @Autowired
    private EstadoPagoServicio estadoPagoServicio;
    
    @GetMapping("/lista")
    public List<EstadoPago> consultarTodo(){
        return estadoPagoServicio.listarEstadoPago();
    }
    
    @PostMapping
    public EstadoPago insertar(@RequestBody EstadoPago nuevoEst) {
        return estadoPagoServicio.save(nuevoEst);
    }
    
    @DeleteMapping(path = "/{id}") 
    public String eliminar(@PathVariable Integer id) {
        try {
            estadoPagoServicio.delete(id);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }
    
    @PutMapping
    public EstadoPago actualizar(@RequestBody EstadoPago estadoPagoModificar) {
        return estadoPagoServicio.actualizar(estadoPagoModificar);
    }
}
