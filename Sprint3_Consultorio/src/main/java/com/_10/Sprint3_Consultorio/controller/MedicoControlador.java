
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.Medico;
import com._10.Sprint3_Consultorio.service.MedicoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Breyner payares
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/medico")
public class MedicoControlador {
    
    @Autowired
    private MedicoServicio medicoServicio;
    
    @GetMapping("/lista")
    public List<Medico> MostrarMedico(){
        return medicoServicio.mostrarTodo();
    }
    
    @PostMapping
    public Medico insertar(@RequestBody Medico MedicoNuevo){
        return medicoServicio.guardarMedico(MedicoNuevo);
    }
    @PutMapping
    public Medico ActualizarMedico(@RequestBody Medico medicoActualizar){
        return medicoServicio.guardarMedico(medicoActualizar);
    }
    
    @DeleteMapping(path = "/{id}")
    public String eliminarMedico(@PathVariable Integer id){
        try {
            medicoServicio.eliminar(id);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
    
    @GetMapping("/buscar")
    public List<Medico> buscarPorMedico(String medico){
        return medicoServicio.buscarPorNombreMedico(medico);
    }
}
