/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.Paciente;
import com._10.Sprint3_Consultorio.model.User;
import com._10.Sprint3_Consultorio.service.PacienteServicios;
import com._10.Sprint3_Consultorio.service.UserServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author orlando
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/paciente")
public class PacienteControlador {
    @Autowired
    private PacienteServicios pacienteServicio;

    @GetMapping("/lista")
    public List<Paciente> consultarTodoPaciente() {
        return pacienteServicio.findAll();
    }

    @PostMapping
    public Paciente insertar(@RequestBody Paciente pacienteNuevo) {
        return pacienteServicio.guardarPaciente(pacienteNuevo);
    }

    @PutMapping
    public Paciente ActualizarPaciente(@RequestBody Paciente pacienteActualizar) {
        return pacienteServicio.guardarPaciente(pacienteActualizar);
    }

    @GetMapping("/id/{id}")
    public Paciente buscarPorIdPaciente(@PathVariable(value = "id") int idPaciente) {
        return pacienteServicio.buscarPorId(idPaciente);

    }

    @DeleteMapping(path = "/{id}")
    public String eliminarPacienteId(@PathVariable Integer id) {
        try {
            pacienteServicio.eliminarPacientePorId(id);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
}
