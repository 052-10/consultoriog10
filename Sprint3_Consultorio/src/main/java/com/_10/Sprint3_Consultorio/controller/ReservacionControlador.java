/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.Reservacion;
import com._10.Sprint3_Consultorio.model.Rol;
import com._10.Sprint3_Consultorio.service.ReservacionServicio;
import com._10.Sprint3_Consultorio.service.RolServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author orlando
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/reservacion")
public class ReservacionControlador {

    @Autowired
    private ReservacionServicio reservacionServicio;

    @GetMapping("/lista")
    public List<Reservacion> consultarTodoReservacion() {
        return reservacionServicio.findAll();
    }

    @PostMapping
    public Reservacion insertar(@RequestBody Reservacion ReservacionNueva) {
        return reservacionServicio.guardarReservacion(ReservacionNueva);
    }

    @PutMapping
    public Reservacion ActualizarReservacion(@RequestBody Reservacion ReservacionActualizar) {
        return reservacionServicio.guardarReservacion(ReservacionActualizar);
    }

    @GetMapping("/id/{id}")
    public Reservacion buscarPorIdReservacion(@PathVariable(value = "id") int idReservacion) {
        return reservacionServicio.buscarPorId(idReservacion);

    }

    @DeleteMapping(path = "/{id}")
    public String eliminarReservacionId(@PathVariable Integer id) {
        try {
            reservacionServicio.eliminarReservacionPorId(id);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
}
