/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.Rol;
import com._10.Sprint3_Consultorio.service.RolServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author orlando
 */
//le indicamos que vamos a recibir peticiones, que es un controlador
@RestController

//le indicamos de donde viene las peticiones
@CrossOrigin("*")

//Le indicamos como va encontrar en la url, es el que identifica mi navegador
@RequestMapping("/rol")
public class RolController {

    @Autowired
    private RolServicio rolServicio;

    @GetMapping("/lista")
    public List<Rol> consultarTodoRol() {
        return rolServicio.findAll();
    }

    @PostMapping
    public Rol insertar(@RequestBody Rol rolNuevo) {
        return rolServicio.guardarRol(rolNuevo);
    }

    @PutMapping
    public Rol ActualizarRol(@RequestBody Rol rolActualizar) {
        return rolServicio.guardarRol(rolActualizar);
    }

    @GetMapping("/id/{id}")
    public Rol buscarPorIdRol(@PathVariable(value = "id") int idRol) {
        return rolServicio.buscarPorId(idRol);

    }

    @DeleteMapping(path = "/{id}")
    public String eliminarRolId(@PathVariable Integer id) {
        try {
            rolServicio.eliminarRolPorId(id);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }

    /*
    @GetMapping("/buscar")
    public List<Rol> buscarPorMedico(String nombreRol){
        return rolServicio..buscarPorNombreMedico(nombreRol);
    }
     */
}
