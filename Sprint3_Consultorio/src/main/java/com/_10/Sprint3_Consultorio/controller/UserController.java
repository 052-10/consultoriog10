/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.controller;

import com._10.Sprint3_Consultorio.model.User;
import com._10.Sprint3_Consultorio.service.UserServicio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author orlando
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/useracceso")
public class UserController {

    @Autowired
    private UserServicio userServicio;

    @GetMapping
    public User login(String email, String password) {
        return userServicio.login(email, password);
    }

    @GetMapping("/hola")
    public String hola() {
        return "hola";
    }

    @GetMapping("/lista")
    public List<User> consultarTodoUser() {
        return userServicio.findAll();
    }

    @PostMapping
    public User insertar(@RequestBody User userNuevo) {
        return userServicio.guardarUser(userNuevo);
    }

    @PutMapping
    public User ActualizarUser(@RequestBody User userActualizar) {
        return userServicio.guardarUser(userActualizar);
    }

    @PutMapping("/id/{id}")
    public User ActualizarUserId(@PathVariable int idUser, @RequestBody User newUser) {
        User usuario = this.userServicio.buscarPorId(idUser);
        usuario.setUsername(newUser.getUsername());
        usuario.setPassword(newUser.getPassword());
        usuario.setEmail(newUser.getEmail());
        usuario.setIdRol(newUser.getIdRol());
        return userServicio.guardarUser(usuario);
    }

    @GetMapping("/id/{id}")
    public User buscarPorIdUser(@PathVariable int idUser) {
        return this.userServicio.buscarPorId(idUser);

    }

    @DeleteMapping(path = "/{id}")
    public String eliminarUserId(@PathVariable Integer id) {
        try {
            userServicio.eliminarUserPorId(id);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
}
