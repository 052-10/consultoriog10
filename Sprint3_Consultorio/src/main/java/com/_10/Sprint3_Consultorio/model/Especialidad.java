
package com._10.Sprint3_Consultorio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Breyner Payares
 */

@Table
@Entity(name="especialidad")
public class Especialidad implements Serializable {
    
    @Id // llave primaria
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    @Column(name="id") //nombre que recibe en la columna
    private Integer id;
    
    @Column(name="nombre_especialidad")
    private String nombreEspecialidad;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreEspecialidad() {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad) {
        this.nombreEspecialidad = nombreEspecialidad;
    }
}
