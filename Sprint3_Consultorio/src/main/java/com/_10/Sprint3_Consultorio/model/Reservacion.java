/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author orlando
 */
@Table
@Entity(name = "reservacion")
public class Reservacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Integer id;

    @Column(name = "asunto_cita")
    private String asuntoCita;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "descripcion")
    private String descripcion;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "fecha_cita")
    private String fechaCita;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "hora_cita")
    private String horaCita;

    @Column(name = "sintomas")
    private String sintomas;

    @Column(name = "enfermedad_padecida")
    private String enfermedadPadecida;
    
     @Column(name = "medicamentos")
    private String medicamentos;

    //Especificamos la relacion de mucho a uno
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User idUser;
    
    @ManyToOne
    @JoinColumn(name = "id_paciente")
    private Paciente idPaciente;
    
    @ManyToOne
    @JoinColumn(name = "id_medico")
    private Medico idMedico;
    
     @ManyToOne
    @JoinColumn(name = "id_estado_pago")
    private EstadoPago idEstadoPago;
     
       @ManyToOne
    @JoinColumn(name = "id_estado_cita")
    private EstadoCita idEstadoCita;

    @Column(name = "precio_cita")
    private Integer precioCita;
    
      @Column(name = "solicita_por_web")
    private Integer solicitarPorWeb;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsuntoCita() {
        return asuntoCita;
    }

    public void setAsuntoCita(String asuntoCita) {
        this.asuntoCita = asuntoCita;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public String getEnfermedadPadecida() {
        return enfermedadPadecida;
    }

    public void setEnfermedadPadecida(String enfermedadPadecida) {
        this.enfermedadPadecida = enfermedadPadecida;
    }

    public String getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(String medicamentos) {
        this.medicamentos = medicamentos;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    public Paciente getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Paciente idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Medico getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Medico idMedico) {
        this.idMedico = idMedico;
    }

    public EstadoPago getIdEstadoPago() {
        return idEstadoPago;
    }

    public void setIdEstadoPago(EstadoPago idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public EstadoCita getIdEstadoCita() {
        return idEstadoCita;
    }

    public void setIdEstadoCita(EstadoCita idEstadoCita) {
        this.idEstadoCita = idEstadoCita;
    }

    public Integer getPrecioCita() {
        return precioCita;
    }

    public void setPrecioCita(Integer precioCita) {
        this.precioCita = precioCita;
    }

    public Integer getSolicitarPorWeb() {
        return solicitarPorWeb;
    }

    public void setSolicitarPorWeb(Integer solicitarPorWeb) {
        this.solicitarPorWeb = solicitarPorWeb;
    }
      
      

}
