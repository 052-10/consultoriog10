
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.EspecialidadDao;
import com._10.Sprint3_Consultorio.model.Especialidad;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Breyner Payares
 */

@Service
public class EspecialidadServicio {
    
    @Autowired
    private EspecialidadDao especialidadDao;
    
    public Especialidad save(Especialidad nuevaEspecialidad){
        return especialidadDao.save(nuevaEspecialidad);
    }
    
    public void delete(int id){
        especialidadDao.delete(especialidadDao.findById(id).get());
    }
    
    public void delete(Especialidad especialidadEliminar){
        especialidadDao.delete(especialidadEliminar);
    }
    
    public List<Especialidad> listarEspecialidad(){
        return especialidadDao.findAll();
    }
    
    public Especialidad actualizar(Especialidad esp){
        return especialidadDao.save(esp);
    }
}
