
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.EstadoCitaDao;
import com._10.Sprint3_Consultorio.model.EstadoCita;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Breyner Payares
 */

@Service
public class EstadoCitaServicio {
   
    @Autowired
    private EstadoCitaDao estadoCitaDao;
    
    public EstadoCita save(EstadoCita nuevoEstadoCita){
        return estadoCitaDao.save(nuevoEstadoCita);
    }
    
    public void delete(int id){
        estadoCitaDao.delete(estadoCitaDao.findById(id).get());
    }
    
    public void delete(EstadoCita estadoCitaEliminar){
        estadoCitaDao.delete(estadoCitaEliminar);
    }
    
    public List<EstadoCita> listarEstadoCita(){
        return estadoCitaDao.findAll();
    }
    
    public EstadoCita actualizar(EstadoCita esp){
        return estadoCitaDao.save(esp);
    }
}
