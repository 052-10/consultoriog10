
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.EstadoPagoDao;
import com._10.Sprint3_Consultorio.model.EstadoPago;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Breyner Payares
 */

@Service
public class EstadoPagoServicio {
    
    @Autowired
    private EstadoPagoDao estadoPagoDao;
    
    public EstadoPago save(EstadoPago nuevoEstadoPago){
        return estadoPagoDao.save(nuevoEstadoPago);
    }
    
    public void delete(int id){
        estadoPagoDao.delete(estadoPagoDao.findById(id).get());
    }
    
    public void delete(EstadoPago estadoPagoEliminar){
        estadoPagoDao.delete(estadoPagoEliminar);
    }
    
    public List<EstadoPago> listarEstadoPago(){
        return estadoPagoDao.findAll();
    }
    
    public EstadoPago actualizar(EstadoPago esp){
        return estadoPagoDao.save(esp);
    }
}
