
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.MedicoDao;
import com._10.Sprint3_Consultorio.model.Medico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Breyner Payares
 */

@Service
public class MedicoServicio {
    
    @Autowired
    private MedicoDao medicoDao;
    
    public List<Medico> mostrarTodo(){
        return medicoDao.findAll();
    }
    
        public Medico guardarMedico(Medico MedicoNuevo){
        return medicoDao.save(MedicoNuevo);
    }
        
    public Medico buscarPorId(int id){
        return medicoDao.findById(id).get();
    }
    
    public void eliminar(int id){
        medicoDao.delete(medicoDao.findById(id).get());
    }
    
    public List<Medico> buscarPorNombreMedico(@RequestParam String producto){
        return medicoDao.searchBynombreMedico(producto);
    } 
}
