/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.PacienteDao;
import com._10.Sprint3_Consultorio.Dao.UserDao;
import com._10.Sprint3_Consultorio.model.Paciente;
import com._10.Sprint3_Consultorio.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author orlando
 */
@Service
public class PacienteServicios {

    @Autowired
    private PacienteDao pacienteDao;

    //creamos los metodos para guardar el usuario en la base de datos
    public Paciente guardarPaciente(Paciente nuevopaciente) {
        return pacienteDao.save(nuevopaciente);
    }

    //actualizo el perfil
    public Paciente actualizarPaciente(Paciente actualizarPaciente) {
        return pacienteDao.save(actualizarPaciente);
    }

    //Muetro todos los perfiles
    public List<Paciente> findAll() {
        return (List<Paciente>) pacienteDao.findAll();
    }

    public Paciente buscarPorId(int idPaciente) {
        return pacienteDao.findById(idPaciente).get();
    }

    //Eliminar por id
    public void eliminarPacientePorId(int idPaciente) {
        pacienteDao.delete(pacienteDao.findById(idPaciente).get());
    }

    //eliminar por el nombre
    public void eliminarPacientePorNombre(Paciente pacienteEliminar) {
        pacienteDao.delete(pacienteEliminar);
    }

}
