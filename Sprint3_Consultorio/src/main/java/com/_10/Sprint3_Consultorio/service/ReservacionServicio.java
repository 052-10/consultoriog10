/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.ReservacionDao;
import com._10.Sprint3_Consultorio.Dao.RolDao;
import com._10.Sprint3_Consultorio.model.Reservacion;
import com._10.Sprint3_Consultorio.model.Rol;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author orlando
 */
@Service
public class ReservacionServicio {
    @Autowired
    private ReservacionDao reservacionDao;
     
        //creamos los metodos para guardar el perfil en la base de datos
    public Reservacion guardarReservacion(Reservacion nuevaReservacion) {
        return reservacionDao.save(nuevaReservacion);
    }
    
  //actualizo el reservacion
    public Reservacion actualizarReservacion(Reservacion actualizarReservacion) {
        return reservacionDao.save(actualizarReservacion);
    }
    
    //Muetro todas las Reservaciones
     public List<Reservacion> findAll() {
        return (List<Reservacion>) reservacionDao.findAll();
    }
     
      public Reservacion buscarPorId(int idReservacion) {
        return reservacionDao.findById(idReservacion).get();
    }
      
      
      
      //Eliminar por id
    public void eliminarReservacionPorId(int idReservacion) {
        reservacionDao.delete(reservacionDao.findById(idReservacion).get());
    }

    //eliminar por el nombre
    public void eliminarReservacionPorNombre(Reservacion ReservacionEliminar) {
        reservacionDao.delete(ReservacionEliminar);
    }
    
}
