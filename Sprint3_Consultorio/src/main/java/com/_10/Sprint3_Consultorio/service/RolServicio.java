/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.model.Rol;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com._10.Sprint3_Consultorio.Dao.RolDao;

/**
 *
 * @author orlando
 */
//le indico que es un servicio
@Service
public class RolServicio {
     @Autowired
    private RolDao rolDao;
     
        //creamos los metodos para guardar el perfil en la base de datos
    public Rol guardarRol(Rol nuevoRol) {
        return rolDao.save(nuevoRol);
    }
    
  //actualizo el perfil
    public Rol actualizarRol(Rol actualizarRol) {
        return rolDao.save(actualizarRol);
    }
    
    //Muetro todos los perfiles
     public List<Rol> findAll() {
        return (List<Rol>) rolDao.findAll();
    }
     
      public Rol buscarPorId(int idRol) {
        return rolDao.findById(idRol).get();
    }
      
      
      
      //Eliminar por id
    public void eliminarRolPorId(int idRol) {
        rolDao.delete(rolDao.findById(idRol).get());
    }

    //eliminar por el nombre
    public void eliminarRolPorNombre(Rol rolEliminar) {
        rolDao.delete(rolEliminar);
    }
    
    
}
