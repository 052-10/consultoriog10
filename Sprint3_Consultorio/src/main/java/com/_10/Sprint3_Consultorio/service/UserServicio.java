/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com._10.Sprint3_Consultorio.service;

import com._10.Sprint3_Consultorio.Dao.UserDao;
import com._10.Sprint3_Consultorio.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




/**
 *
 * @author orlando
 */
@Service
public class UserServicio {

    @Autowired
    private UserDao userDao;

    public UserServicio(UserDao userDao) {
        this.userDao = userDao;
    }
    
    public User login(String usuario , String password){
        return userDao.login(usuario, password);
    }
    
    //creamos los metodos para guardar el usuario en la base de datos
    public User guardarUser(User nuevoUser) {
        return userDao.save(nuevoUser);
    }

    //actualizo el perfil
    public User actualizarUser(User actualizarUser) {
        return userDao.save(actualizarUser);
    }

    //Muetro todos los perfiles
    public List<User> findAll() {
        return (List<User>) userDao.findAll();
    }

    public User buscarPorId(int idUser) {
        return userDao.findById(idUser).get();
    }

    //Eliminar por id
    public void eliminarUserPorId(int idUser) {
        userDao.delete(userDao.findById(idUser).get());
    }

    //eliminar por el nombre
    public void eliminarUserPorNombre(User userEliminar) {
        userDao.delete(userEliminar);
    }

}
